package ai.cequence.spring.batch.springbatchdemo

import org.springframework.batch.core.JobExecution
import org.springframework.batch.core.listener.JobExecutionListenerSupport
import org.springframework.stereotype.Component

@Component
class LoglineJobCompletionNotificationListener: JobExecutionListenerSupport() {
    override fun afterJob(jobExecution: JobExecution) {
        println("Jobs done!" + jobExecution.exitStatus.exitDescription)
        super.afterJob(jobExecution)
    }
}