package ai.cequence.spring.batch.springbatchdemo

import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.launch.support.RunIdIncrementer
import org.springframework.batch.item.ItemProcessor
import org.springframework.batch.item.ItemWriter
import org.springframework.batch.item.file.FlatFileItemReader
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder
import org.springframework.batch.item.file.mapping.PassThroughFieldSetMapper
import org.springframework.batch.item.file.mapping.PassThroughLineMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource


@Configuration
@EnableBatchProcessing
class BatchConfiguration {
    @Autowired
    var jobBuilderFactory: JobBuilderFactory? = null

    @Autowired
    var stepBuilderFactory: StepBuilderFactory? = null

    @Bean
    fun reader(): FlatFileItemReader<String> {
        return FlatFileItemReaderBuilder<String>()
                .name("loglineReader")
                .comments("#")
                .lineMapper(passthroughLineMapper())
                .resource(ClassPathResource("http.log"))
                .build()
    }


    @Bean
    fun passthroughMapper(): PassThroughFieldSetMapper {
       return PassThroughFieldSetMapper()
    }

    @Bean
    fun passthroughLineMapper(): PassThroughLineMapper {
       return PassThroughLineMapper()
    }

    @Bean
    fun writer(): ItemWriter<HttpLog> {
        return  ItemWriter {items ->
            items.forEach { item ->
                run {
                    if (item.type != HttpLog.Type.HTTP) {
                        println(item.type)
                    }
                }
            }
        }
    }


    @Bean
    fun loglineProcessor2(): ItemProcessor<String, HttpLog> {
       return ItemProcessor { item ->
           val raw = item.split(("\t"))
           HttpLog(raw).also {log ->
               if (raw.size != 64) {
                   log.type == HttpLog.Type.SSL
               }
               log.method = raw[7]
               log.host = raw[8]
               log.uri = raw[9]
               log.userAgent = raw[11]
               log.headerAccept = raw[20]
               if (raw[12] != "-") {
                   log.responseCode = raw[12].toInt()
               }
               println(log.uri)
           }
       }
    }

    @Bean
    fun importUserJob(notificationListener: LoglineJobCompletionNotificationListener,
                      step1: Step): Job {
        return jobBuilderFactory!!.get("importUserJob")
                .incrementer(RunIdIncrementer())
                .listener(notificationListener)
                .flow(step1)
                .end()
                .build()
    }

    @Bean
    fun step1(): Step {
        return stepBuilderFactory!!.get("step1")
                .chunk<String, HttpLog>(20)
                .reader(reader())
                .processor(loglineProcessor2())
                .writer(writer())
                .build()
    }

}

