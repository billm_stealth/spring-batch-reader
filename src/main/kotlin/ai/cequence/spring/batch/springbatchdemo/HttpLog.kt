package ai.cequence.spring.batch.springbatchdemo

class HttpLog(raw: List<String>) {
    var userAgent: String? = null
    var uri: String = ""
    var method: String = ""
    var host: String = ""
    var type: Type = Type.HTTP
    var responseCode: Int? = null
    var headerAccept: String? = null

    enum class Type {
        SSL, HTTP
    }

    override fun toString(): String {
        return "HttpLog(userAgent=$userAgent, uri='$uri', method='$method', host='$host', type=$type, responseCode=$responseCode, headerAccept=$headerAccept)"
    }
}

